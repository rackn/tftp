module gitlab.com/rackn/tftp/v3

go 1.16

require (
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20210903162142-ad29c8ab022f
	golang.org/x/sys v0.0.0-20210903071746-97244b99971b // indirect
)
